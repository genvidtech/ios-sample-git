// Copyright 2016-2019 Genvid Technologies Inc. All Rights Reserved.

function newTimeSmoother(inTime = null, clientTimestamp = new Date(), maxEstimationDuration = Infinity) {
    return new TimeSmoother(inTime, clientTimestamp, maxEstimationDuration);
};

class TimeSmoother {
    constructor(inTime = null, clientTimestamp = new Date(), maxEstimationDuration = Infinity) {
        this.lastObservedTime = inTime || 0;
        this.lastObservedTimeClient = clientTimestamp || new Date();
        this.maxEstimationDuration = maxEstimationDuration;
    }

    set(time, clientTimestamp = new Date()) {
        if (time !== this.lastObservedTime) {
            this.lastObservedTime = time;
            this.lastObservedTimeClient = clientTimestamp || new Date();
        }
    }

    get(clientTimestamp = new Date()) {
        clientTimestamp = clientTimestamp || new Date();
        let clientTime = clientTimestamp.getTime();
        let lastTime = this.lastObservedTimeClient.getTime();
        let delta = (clientTime - lastTime) / 1000;
        delta = Math.min(delta, this.maxEstimationDuration);
        return this.lastObservedTime + delta;
    }

    set_get(time, clientTimestamp = new Date()) {
        this.set(time, clientTimestamp);
        return this.get(clientTimestamp);
    }

    set_max_estimation_duration(duration) {
        this.maxEstimationDuration = duration;
    }
}
