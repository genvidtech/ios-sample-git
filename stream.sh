#!/bin/bash
FILE="Enter path to .mp4 video file"# i.e. "sintel-1280-surround.mp4"
DST="out.flv"

# Enter your secret YouTube Streaming key here.
YOUTUBE_KEY="Enter Youtube Key Here"
YOUTUBE_URL="rtmp://a.rtmp.youtube.com/live2/${YOUTUBE_KEY}"
# Link to view the video from channel
# https://www.youtube.com/channel/{YOUTUBE_CHANNEL_ID}/live

# Enter your secret Twitch Streaming key here.
TWITCH_KEY="Enter Twitch Key Here (ie.: live_xxxxx...)"
TWITCH_URL="rtmp://live.twitch.tv/app/${TWITCH_KEY}"
# Link to view the video from channel
# https://www.twitch.tv/{TWITCH_USERNAME}/

# Some filter to burn both GMT time and PTS into the video.
FILTER_BURN_TIMECODE="drawtext=text=GMT=%{gmtime} PTS=%{pts\\\\:hms}: fontcolor='red': fontsize=64: alpha=0.7:"

# You can only stream to one platform at a time. When uncommenting a line make sure others are commented.
# Stream to YouTube.
ffmpeg -re -stream_loop -1 -i "${FILE}" -vf "${FILTER_BURN_TIMECODE}" -c:v libx264 -b:v 2.5M -c:a aac -f flv "${YOUTUBE_URL}"

# Stream to Twitch.
#ffmpeg -re -stream_loop -1 -i "${FILE}" -vf "${FILTER_BURN_TIMECODE}" -c:v libx264 -b:v 2.5M -c:a aac -f flv "${TWITCH_URL}"

# Stream to file.
#ffmpeg -y -stream_loop -1 -i "${FILE}" -vf "${FILTER_BURN_TIMECODE}" -c:v libx264 -b:v 2.5M -c:a aac -f flv "${DST}"
