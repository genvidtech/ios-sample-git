// Player Initialization made when user selects player.

let tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
let debug = false;
let srcChanged = false;
let youtubeApiReady = false;
let youtubePlayerReady = false;
let twitchPlayerReady = false;
let channelUrl = "https://www.youtube.com/embed/live_stream?channel=" + configs.youtube.channelId +
    "&enablejsapi=1&loop=1&autoplay=1&controls=0&playsinline=1&disablekb=1";
let player, playerTW, currentElem, mediaRefElem, smootherElem, durationElem, playerSelected, logElem, twStats;

/////// TWITCH ///////
let options = {
    width: configs.twitch.width,
    height: configs.twitch.height,
    channel: configs.twitch.channelId,
    playsinline: true
};
let lastCurrentTime = -1;
let ntpTimeOffset = 0;
//////////////////////

// Replace the 'player' element with an <iframe> and
// YouTube player after the API code downloads.
function onYouTubeIframeAPIReady() {
    log("onYouTubeIframeAPIReady Ready!!");
    youtubeApiReady = true;
}
function initYTPlayer() {
    log("In initYTPlayer");
    player = new YT.Player('playerYT', {
        height: configs.youtube.height,
        width: configs.youtube.width,
        frameBorder: 0,
        enablejsapi: 1,
        // Todo: Create function to getVideo ID
        // PlayerVars works only if you have the VideoId from the get go
        //playerVars: { 'autoplay': 1, 'controls': 0, loop: 1, enablejsapi: 1, playsinline: 1, disablekb: 1 },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onStateChange,
            'onPlaybackQualityChange': onQualityChange,
            'onPlaybackRateChange': onRateChange,
            'onError': onError,
            'onApiChange': onApiChange
        }
    });
    log(player);
}

function initTWPlayer() {
    playerTW = new Twitch.Player("playerTW", options);
    playerTW.setVolume(0.5);
    playerTW.addEventListener(Twitch.Player.READY, onPlayerTWReady);
    playerTW.addEventListener(Twitch.Player.PLAYING, onPlayerTWPlaying);
    playerTW.addEventListener(Twitch.Player.ENDED, onPlayerTWEnded);
    playerTW.addEventListener(Twitch.Player.PAUSE, onPlayerTWPause);
}

function onPlayerReady(event) {
    if (!srcChanged) {
        log("srcChanged: " + srcChanged + " | PlayerReady Ready!!");
        log(event);
        let playerYT = player.getIframe();
        playerYT.setAttribute("allow", "autoplay");
        playerYT.setAttribute("src", channelUrl);
        log(playerYT);
        let playerParent = document.querySelectorAll("#playerYT");
        log(playerParent);
        srcChanged = true;
        displayTime(event);
    } else {
        // Initialize DOM elements
        log("srcChanged: " + srcChanged + " | Initialinzing DOM elements!!");
        currentElem = document.getElementById("currentTime");
        mediaRefElem = document.getElementById("mediaReferenceTime");
        smootherElem = document.getElementById("smoother");
        durationElem = document.getElementById("duration");
        logElem = document.getElementById("console");
        if (debug) {
            logElem.style.display = 'block';
        }
        youtubePlayerReady = true;
    }
}

function onStateChange(event) {
    log("***  PlayerStateChanged !!");
    log(event);
}

function displayTime(event) {
    let smootherVal, timeSmoother;
    setInterval(function() {
        if (youtubePlayerReady && player.getPlayerState() != -1 && player.getPlayerState() != 2 && player.getPlayerState() != 5) {
            log("Player State: " + player.getPlayerState());
            let currentTime = player.getCurrentTime();
            if (currentTime && currentTime > 0) {
                currentTime = currentTime;
            }

            let referenceTime = player.getMediaReferenceTime();
            if (referenceTime && referenceTime > 0) {
                referenceTime = referenceTime;
            }

            let duration = player.getDuration();
            if (duration && duration < 0 || duration == Infinity) {
                duration = "---";
            } else {
                duration = duration;
            }

            log("======= DATA =======");
            log(player.getVideoData());
            log("");

            currentElem.innerHTML = secToTime(currentTime);
            mediaRefElem.innerHTML = secToTime(referenceTime);
            durationElem.innerHTML = secToTime(duration);

            if (currentTime) {
                // Code for timeSmoother
                timeSmoother = newTimeSmoother(currentTime);
                if (smootherVal !== undefined) {
                    clearInterval(smootherVal);
                }
                smootherVal = setInterval(function () {
                    smootherElem.innerHTML = secToTime(timeSmoother.get());
                }, 100);
            }
        } else {
            clearInterval(smootherVal);
        }
    }, 1000);
}
function displayTWTime() {
    let smootherVal, timeSmoother;
    setInterval(function() {
        let currentTime = getCurrentTime();
        if (!currentTime || currentTime < 0) {
            currentTime = 0;
        }
        let duration = playerTW.getDuration();
        if (duration && duration < 0 || duration == Infinity) {
            duration = null;
        }

        log("======= DATA =======");
        let playbackStats = playerTW.getPlaybackStats();
        log("*** playbackStats: " + JSON.stringify(Object.getOwnPropertyNames(playbackStats)), "twStats");
        log("backendVersion: " + playbackStats.backendVersion, "twStats");
        log("bufferSize: " + playbackStats.bufferSize, "twStats");
        log("codecs: " + playbackStats.codecs, "twStats");
        log("displayResolution: " + playbackStats.displayResolution, "twStats");
        log("fps: " + playbackStats.fps, "twStats");
        log("hlsLatencyBroadcaster: " + playbackStats.hlsLatencyBroadcaster, "twStats");
        log("hlsLatencyEncoder: " + playbackStats.hlsLatencyEncoder, "twStats");
        log("latencyMode: " + playbackStats.latencyMode, "twStats");
        log("memoryUsage: " + playbackStats.memoryUsage, "twStats");
        log("playbackRate: " + playbackStats.playbackRate, "twStats");
        log("skippedFrames: " + playbackStats.skippedFrames, "twStats");
        log("videoResolution: " + playbackStats.videoResolution, "twStats");
        log("", "twStats");

        currentElem.innerHTML = secToTime(currentTime);
        durationElem.innerHTML = duration ? secToTime(duration) : "N/A";

        if (currentTime) {
            // Code for timeSmoother
            timeSmoother = newTimeSmoother(currentTime);
            if (smootherVal !== undefined) {
                clearInterval(smootherVal);
            }
            smootherVal = setInterval(function() {
                smootherElem.innerHTML = secToTime(timeSmoother.get());
            }, 100);
        }

    }, 1000);
}

function secToTime(duration) {
    return new Date(duration * 1000).toISOString().substr(11, 11);
}

function playPauseVideo() {
    if (playerSelected === "Youtube") {
        if (player.getPlayerState() == 1) {
            player.pauseVideo();
        } else {
            player.playVideo();
        }
    } if (playerSelected === "Twitch") {
        if (playerTW.isPaused()) {
            playerTW.play();
        } else {
            playerTW.pause();
        }
    }
}
function stopVideo() {
    if (playerSelected === "Youtube") {
        if (player.getPlayerState() != 5) {
            player.stopVideo();
        }
    } if (playerSelected === "Twitch") {
        if (!playerTW.isPaused()) {
            playerTW.pause();
        }
    }
}
function muteVideo() {
    if (playerSelected === "Youtube") {
        if (player.isMuted()) {
            player.unMute();
        } else {
            player.mute();
        }
    } if (playerSelected === "Twitch") {
        const isMuted = playerTW.getMuted();
        playerTW.setMuted(!isMuted);
    }
}

function onQualityChange() {
    log("*** onPlaybackQualityChange ***");
}
function onRateChange() {
    log("*** onPlaybackRateChange ***");
}
function onError() {
    log("*** onError ***");
}
function onApiChange(event) {
    log("*** onApiChange ***");
    log(event);
}

function onPlayerTWReady() {
    log("*** onPlayerTWReady ***");
    twitchPlayerReady = true;
    // Initialize DOM elements
    log("Initializing DOM elements!!");
    currentElem = document.getElementById("currentTime");
    mediaRefElem = document.getElementById("mediaReferenceTime");
    smootherElem = document.getElementById("smoother");
    durationElem = document.getElementById("duration");
    logElem = document.getElementById("console");
    twStats = document.getElementById("twStats");
    twStats.style.display = 'block';
}

function onPlayerTWPlaying() {
    log("*** onPlayerTWPlaying ***");
    displayTWTime();
}
function onPlayerTWEnded() {
    log("*** onPlayerTWEnded ***");
}
function onPlayerTWPause() {
    log("*** onPlayerTWPause ***");
}

function getCurrentTime() {
    const playbackStats = this.getPlaybackStats();
    log("HLS Latency Broadcaster: " + playbackStats.hlsLatencyBroadcaster);
    if (playbackStats) {
        const newTime = new Date();
        log("Date: " + newTime);
        newTime.setTime(newTime.getTime() - (playbackStats.hlsLatencyBroadcaster * 1000) + ntpTimeOffset);
        log("setTime: " + newTime);
        lastCurrentTime = newTime.getTime() / 1000;
        log("getTime: " + lastCurrentTime);
    } else {
        lastCurrentTime = -1;
    }
    return lastCurrentTime;
}

function getPlaybackStats() {
    if (twitchPlayerReady && playerTW) {
        return playerTW.getPlaybackStats();
    } else {
        return undefined;
    }
}

function playerSelection(playerType) {
    if (playerSelected === playerType) {
        return;
    } else {
        log(playerType);
        playerSelected = playerType;
        removePreviousPlayer().then(() => {
            if (playerSelected === "Youtube") {
                if (youtubeApiReady) {
                    initYTPlayer();
                }
            } else {
                initTWPlayer();
            }
        });
    }
}
function debugSelection(value) {
    debug = value;
    if (logElem) {
        if (debug) {
            logElem.style.display = "block";
        } else {
            logElem.style.display = "none";
        }
    }
}

function log(message, areaId = null) {
    console.log(message);
    if (debug && logElem) {
        let currMsg = logElem.value;
        if (typeof message === "object") {
            message = parseObject(message);
        }
        logElem.value = currMsg + "\n" + message;
    }
    if (areaId) {
        let area = document.getElementById(areaId);
        let currMsg = area.value;
        area.value = currMsg + "\n" + message;
    }
}

function parseObject(obj) {
    let strObj = "";
    Object.keys(obj).map(key => {
        strObj += key + ": " + obj[key] + "\n";
    });
    return strObj;
}

function removePreviousPlayer() {
    return new Promise(resolve => {
        console.log("Selected Player: " + playerSelected);
        if (playerSelected !== "Youtube") {
            if (youtubePlayerReady) {
                document.getElementById("YtContainer").innerHTML = "<div id=\"playerYT\"></div>";
                srcChanged = false;
                youtubePlayerReady = false;
            }
        } else {
            if (twitchPlayerReady) {
                document.getElementById("playerTW").innerHTML = "";
                twStats.style.display = 'none';
                twitchPlayerReady = false;
            }
        }
        resolve(true);
    });
}
