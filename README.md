## ios-sample

#### Documentation on how to deploy the iOS Sample solution

1 - If FFMPEG is not in your system environment variables:

    - Download FFMPEG from https://ffmpeg.zeranoe.com/builds/
    - Unzip the package
	- Set the 'ffmpeg/bin' directory to your 'path' system variables (you can move the ffmpeg folfer to c:/ffmpeg)
		- Windows
			- In Search, search for and then select: System (Control Panel)
			- Click the Advanced system settings link.
			- Click Environment Variables.
			- In the Edit System Variable (or New System Variable) window,
			    specify the value of the PATH environment variable.
			
2 - Edit the stream.sh file

    **Important**: You can only stream to one platform at a time.
    
    - Enter path to a mp4 video file
        - You can use any mp4 video file stored locally on your machine or use the
          one I downloaded from https://durian.blender.org/download/ (1280 x 544).
    
    - Enter your YOUTUBE KEY
        - Get your Youtube Key:
            - Go to address: https://www.youtube.com/live_dashboard
            - Scroll down until you see the ENCODER SETUP box.
            - Click on the Reveal button, select and copy your key.

    - Enter your TWITCH KEY
        - Get your Twitch key:
            - Go to address: https://www.twitch.tv/broadcast/dashboard/streamkey
            - Click on the 'Copy' button of the Primary Stream key section

    - If you're testing on Youtube, uncomment the 'ffmpeg' line following the "Stream to YouTube" line (22).
        - Make sure to comment the other 'ffmpeg' command lines (25, 28).
    - If you're testing on Twitch, uncomment the 'ffmpeg' line following the "Stream to Twitch" line (25).
        - Make sure to comment the other 'ffmpeg' command lines (22, 28).

3 - Edit the 'configs.js' file

    - Enter your Youtube and Twitch channel IDs.

4 - Play movie

    - In Command Prompt, run the 'stream.sh' file,
        it will steam the movie/clip to the uncommented broadcast output.

5 - Generate server

    - Python must be installed on your computer.
    - Still in Command Prompt run the  command:
        'py -3 -m http.server --bind 0.0.0.0 8080'
        - You can change the last four digits to modify the port to serve to.

6 - Open solution on device (autoplay might not work)

    - To get to the solution's URL you need to find your IP address
        In Command Promt:
          - Run the 'ipconfig' command
          - Use the IPv4 Address as the address to access the solution.
    - Open the address: {your IP address}:{port in Step 5}
    - Select Youtube or Twitch to launch the player.
    
7 - How to detect any sync issues?

    - For Youtube:
        - If the Media Reference Time or the Current Time are not the same
          as the ones displayed in the top of the video.
          -  Media Reference Time should the same as the GMT time
          -  Current Time should be the same as the PTS time
    - For Twitch:
        - If the Current Time shown is not the same as the PTS time displayed
          on top of the video.
